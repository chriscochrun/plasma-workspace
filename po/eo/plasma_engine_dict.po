# translation of plasma_engine_dict.pot to esperanto
# Copyright (C) 2017 Free Software Foundation, Inc.
# This file is distributed under the same license as the plasma-workspace package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
# Minuskloj: ĉ ĝ ĵ ĥ ŝ ŭ   Majuskloj: Ĉ Ĝ Ĵ Ĥ Ŝ Ŭ
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-15 00:20+0000\n"
"PO-Revision-Date: 2023-04-07 23:35+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: dictengine.cpp:67
#, kde-format
msgid "No match found for %1"
msgstr "Neniu kongruo trovita por %1"
