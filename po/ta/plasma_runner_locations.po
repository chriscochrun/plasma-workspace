# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Kishore G <kishore96@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: krunner_locationsrunner\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-01 00:49+0000\n"
"PO-Revision-Date: 2021-06-01 21:44+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: Tamil\n"
"X-Poedit-Country: INDIA\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Generator: Lokalize 21.04.1\n"

#: locationrunner.cpp:33
#, kde-format
msgid ""
"Finds local directories and files, network locations and Internet sites with "
"paths matching :q:."
msgstr ""
":q: என்பதற்கு பொருந்தும் உள்ளமை கோப்புகள் மற்றும் அடைவுகள், பிணைய இடங்கள், மற்றும் "
"இணையதளங்களை கண்டுபிடிக்கும்."

#: locationrunner.cpp:59
#, kde-format
msgid "Open %1"
msgstr "%1 -ஐ திற"

#: locationrunner.cpp:78 locationrunner.cpp:82
#, kde-format
msgid "Launch with %1"
msgstr "%1 -ஆல் இயக்கு"

#: locationrunner.cpp:88
#, kde-format
msgid "Go to %1"
msgstr "%1 -க்கு செல்"

#: locationrunner.cpp:96
#, kde-format
msgid "Send email to %1"
msgstr "%1 -க்கு அஞ்சல் அனுப்பு"

#~ msgid "Locations"
#~ msgstr "இடங்கள்"
